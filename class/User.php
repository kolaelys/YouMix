<?php


include_once(__DIR__.'/DB.php');

class User{

    public $pseudo="Entrez votre pseudo";
    public $password="Entrez votre mot de passe";
    public $mail= "Entrez votre adresse mail";
    


    function __construct($pseudo, $password, $mail){

        $this->setPseudo($pseudo);
        $this->setPassword($password);
        $this->setMail($mail);

    }

    public function getPseudo(){
        return $this->pseudo;

    }

    public function getPassword(){
        return $this->password;
    }
    
    public function getMail(){
        return $this->mail;
    }

    public function setPseudo($value){
        $this->pseudo=($value);
    }

    public function setPassword($value){
        $this->password=($value);
    }

    public function setMail($value){
        $this->mail=($value);
    }

    function addUser($pseudo, $password, $mail){
        $bdd=Db::connexion();
        $req=$bdd->prepare('INSERT INTO User (`pseudo`,`password`,`mail`) VALUES (:pseudo, :password, :mail)');
        $return = $req->execute(array(
            'pseudo'=>$pseudo,
            'password'=>$password,
            'mail'=>$mail
        ));
        session_start();
    
    }
     
     static function allUser(){ /*Toutes les donées pour l'inscription */
        $bdd=Db::connexion();
        $request= $bdd->query("SELECT * FROM `User`")->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "User",["","",""]);
        return $request;
     }

      static function userCo($pseudo, $password){  //fontion qui vérifie si les infos de la co correspondent à un utilisateur inscrit
        $bdd=Db::connexion();
        $req = $bdd->prepare("SELECT * FROM User WHERE pseudo = :pseudo  AND password = :password");
        $return = $req->execute(array(
            'pseudo' => $pseudo,
            'password' => $password,
            
        ));

        $resultat = $req->fetch();
        
        if (!$resultat)
        {   
            echo 'Mauvais identifiant ou mot de passe !';
            header('Location:../index.php');
        }
        else
        {   session_start();
            echo $resultat['id'] ;
            $_SESSION['id']= $resultat['id'];
            echo "sessionID:".$_SESSION['id']; 
            echo 'Vous êtes connecté !';
            header('Location:../acceuil/acceuil.php');
            
        }
        }        
       
    }
 
?>