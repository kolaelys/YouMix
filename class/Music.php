
<?php

class Music{

    private $titleM="Titre de la musique: ";
    private $link= "Entrez le lien de votre vidéo: ";
    public $id;
    private $UserVideoId;
    
    

    public function __construct($titleM,$link,$UserVideoId){
        $this->setTitleM($titleM);
        $this->setLink($link);
        $this->setUserVideoId($UserVideoId);
}

    
    public function getTitleM(){
        return $this->titleM;
    }

    public function getLink(){
        return $this->link;
    }

    public function getId(){
        return $this->id;
    }

    public function getLinked(){
        return substr($this->getLink(),32,11); /* Recupérer l'id du lien selectionné => $this*/
    }

    public function getUserVideoId(){
        return $this->UserVideoId;
    }
    
    public function setTitleM($value){
            $this->titleM=$value;
         }

         public function setLink($src){
             $this->link=$src;
         }

    public function setUserVideoId($value){
         $this->UserVideoId=$value;
    }

         function addMusic($titleM,$link, $UserVideoId){
            $bddM=DbM::connexion();
            $req=$bddM->prepare('INSERT INTO Music (`titleM`,`link`,`UserVideoId`) VALUES (:titleM, :link, :UserVideoId)');
            $return = $req->execute(array(
                'titleM'=>$titleM,
                'link'=>$link,
                'UserVideoId'=>$UserVideoId
            ));
            echo '$userVideoId: '.$UserVideoId."<br/>";
        
        }


        static function allMusic(){ /* Recuperer la liste des musics dans la db*/
            $bddM=DbM::connexion();
            $request= $bddM->query("SELECT * FROM `Music`")->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Music",["","",""]);
            return $request;
         }

         static function musicExist($titleM, $link){ //Verifier si la musique exiqte dans la db
            $bddM=DbM::connexion();
            $req = $bddM->prepare("SELECT * FROM Music WHERE titleM = :titleM AND link = :link");
            $return = $req->execute(array(
                'titleM' => $titleM,
                'link' => $link
            ));

            $result = $req->fetch();
        
            if (!$result)
            {
                echo 'La vidéo existe déjà !';
            }
            else
            {
            
                echo 'Vidéo ajoutée!';
            }
            }
        
        
    }
?>