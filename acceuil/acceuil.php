<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>Acceuil</title>
        <meta charset="utf-8">
        
        <link href="css/acceuil.css" rel="stylesheet" type="text/css">
        <script src="../js/index.js" defer></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                crossorigin="anonymous">
        </script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
                integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
                crossorigin="anonymous">
        </script>
        <style>
                @import url('https://fonts.googleapis.com/css?family=Cookie');
        </style>
        <style>
                @import url('https://fonts.googleapis.com/css?family=Lobster+Two');
        </style>
    </head>
    
<body>

  <?php 
   
      include_once(__DIR__.'/../class/Music.php');
      include_once(__DIR__.'/../class/User.php');
      include_once(__DIR__.'/../forms/DBM.php');
      //$Music= Music::allMusic();  
      
      $id = $_SESSION['id'];
      $dataM = DbM::connexion()->query("SELECT * FROM Music WHERE UserVideoId=$id")->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Music", ["","",""]);
  ?> 
      
  <div class="main">

    <header>
    <div class="youMix"><h2>YOUMIX</h2></div>
    <div class="userName"><h2>BIENVENUE</h2></div>
    </header>
    <h1><?php echo 'Your Playlist' ?></h1>
    <div class="videoList">

    <div class="video">
        <h3>Vos vidéos: </h3>
        <?php 
              $arrayLinked = [];

              foreach($dataM as $music){
              $arrayLinked[] = $music->getLinked();
              $titleM = $music->getTitleM();
              $link= $music->getLink();
              $linked= $music->getLinked();
              $musicId= $music->getID();
              $UserVideoId= $music->getUserVideoId();
        ?>

            <div class="visible" data-id="<?php echo $linked;?>">
                         
              <?php echo $titleM."<br/>";
               ?>
               <form action="../forms/deleteMusic.php" method="POST">
               <input type="hidden" name="id" value="<?php echo $musicId; ?> ">
               <input  type="image" src="delete.png" value="submit" class="delete">
               </form>

            </div>
              <?php } ?>

      </div>

      <!-- ********* API YOUTUBE ***************** -->

      <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->

      <div class="playVideo">

        <div id="player">

        </div>

        <script>
          <?php 
            echo 'var listVideo = ' . json_encode($arrayLinked) . ';';
          ?>
          var i = 0;
          var cueVideo = listVideo[i];
          // 2. This code loads the IFrame Player API code asynchronously.
          var tag = document.createElement('script');

          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

          // 3. This function creates an <iframe> (and YouTube player)
          //    after the API code downloads.
          
          var player;
          function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
            height: '360',
            width: '640',
            videoId: cueVideo,
              events: {
              //'onReady': onPlayerReady,
              'onStateChange': onPlayerStateChange
              }

            });
          }

          // 4. The API will call this function when the video player is ready.
          function onPlayerReady(event) {
            event.target.playVideo();
          }

          // 5. The API calls this function when the player's state changes.
          //    The function indicates that when ending a video ,
          //   
          //var done = false;
          
          function onPlayerStateChange(event) {
              if(event.data === YT.PlayerState.ENDED){
                if(i>=(listVideo.length-1)){
                  cueVideo= listVideo[0];
                  player.loadVideoById(cueVideo);
                } 
                else{
                  cueVideo = listVideo[++i];
                  player.loadVideoById(cueVideo);
                }
              }
          }
          
          function stopVideo() {
            player.stopVideo();
          }

        </script>

      </div>
    
        <!-- ******************************************** -->

    </div>
 
    <div class="formAdd_title">

      <h2> Ajouter une nouvelle vidéo? </h2>
      <div class="formTitle">
        <form action="../forms/addMusic.php" method="POST">
                  
                        <label>Title of the new Video? </label><br/>
                        <input type="text" placeholder="Title?" name="titre" value=""> 
                        <br/>
                        <label>Link?</label><br/>
                        <div class="champs">
                          
                          <input class="add" type="url" placeholder="New Video?" name="link" value="">
                      
                          <input type="hidden" name="musicId" value="'.$ID.'">
                          <input type="hidden" name="UserVideoId" value="'.$UserVideoId.'">
                          
                          <input class="add" type="submit" name="add" value="add to the playlist">
                        </div>
        </form>
      <div>
  </div>    
</body>
</html>