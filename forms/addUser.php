<!DOCTYPE html>
  <html>
    <head>
      <link href="addUser.css" rel="stylesheet" type="text/css">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../materialize-css/css/materialize.min.css"  media="screen,projection"/>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">   
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    </head>
<body>
 <!--Import jQuery before 
 materialize.js-->
 <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
 <script type="text/javascript" src="../materialize-css/js/materialize.min.js"></script>
 <?php

include_once(__DIR__.'/../class/User.php');

/*On recupère les informations dans le formulaire */
$pseudo=htmlspecialchars($_POST['pseudo']);
$password=htmlspecialchars($_POST['password']);

$mail=htmlspecialchars($_POST['mail']);
if(filter_var($mail, FILTER_VALIDATE_EMAIL)){
    $mail = str_replace(array("\n","\r",PHP_EOL),'', $mail);

}

/*On déclare un nouvel objet de la class User*/
$newUser = new User($pseudo, $password, $mail);

/* On l'ajoute à la base de donnée grâce à la fonction addUser() */
$newUser->addUser($pseudo, $password, $mail);
//session_start();


?>

 <header class="masthead text-center text-white">
      <div class="masthead-content">
        <div class="container">
          <h1 class="masthead-heading mb-0"><?php echo "Bienvenue ".$pseudo." vous êtes inscrit(e) sur Youmix!";?></h1>
          <h2 class="masthead-subheading mb-0">"Si la musique nous est si chère, c'est qu'elle est la parole la plus profonde de l'âme, le cri harmonieux de sa joie et de sa douleur."</h2>
          <a href="#" class="btn btn-primary btn-xl rounded-pill mt-5">
              <form action="Connexion.php" method="POST">
                <input type="hidden" name="pseudo" value="<?php echo $pseudo; ?>">
                <input type="hidden" name="password" value="<?php echo $password;  ?>">
                <input type="hidden" name="mail" value="<?php echo $mail;  ?>">
                <button class="btnV"><input class="validate btn btn-secondary btn-lg" type="submit" name="newco" value="Valider votre inscription">
</button>
            </form>

        </a>
        </div>
      </div>
</header>
</body>
</html>





